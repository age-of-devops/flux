# TODO: You should be rather using kube2iam for this

data "aws_iam_policy_document" "external_dns" {
  statement {
    effect = "Allow"
    actions = [
      "route53:ListHostedZones",
      "route53:ListResourceRecordSets",
      "route53:ChangeResourceRecordSets",
      "route53:GetChange",
      "route53:GetHostedZone"
    ]
    resources = ["*"]
  }
}
resource "aws_iam_policy" "external_dns" {
  name_prefix = "externaldns_"
  path = "/"
  policy = "${data.aws_iam_policy_document.external_dns.json}"
}

resource "aws_iam_role_policy_attachment" "demo-node-external-dns" {
  policy_arn = "${aws_iam_policy.external_dns.arn}"
  role       = "${aws_iam_role.demo-node.name}"
}
