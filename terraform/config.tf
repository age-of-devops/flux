provider "aws" {
  profile = "${var.aws_profile}"
  region = "${var.aws_region}"
  allowed_account_ids = ["${var.aws_account_id}"]
}

variable "cluster-name" { }
variable "aws_profile" { }
variable "aws_region" { }
variable "aws_account_id" { }
variable "k8s-version" { default = "1.11" }
