# Flux EKS Meetup Demo

## Prerequisites

* AWS Account
* Domain + Hosted Zone 
* Git Repository

Locally:
* Terraform (https://www.terraform.io/downloads.html)
* Helm (https://github.com/helm/helm)
* kubectl (https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* awscli, with a configured user (https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
* aws-iam-authenticator (https://github.com/kubernetes-sigs/aws-iam-authenticator)
```bash
sudo curl -o /usr/local/bin/aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.10.3/2018-07-26/bin/darwin/amd64/aws-iam-authenticator
sudo chmod +x /usr/local/bin/aws-iam-authenticator
```

## Spin up an EKS Cluster with Terraform

This is heavily inspired by https://github.com/terraform-providers/terraform-provider-aws/tree/master/examples/eks-getting-started

* VPC
* EKS Service
* Autoscaling Group for nodes

```bash
make terraform-apply
```

## Configure kubectl
```bash
export AWS_PROFILE=meetupdemo
aws eks update-kubeconfig --name meetup
#> Updated context arn:aws:eks:eu-central-1:ACCOUNTNUMBER:cluster/meetup in ~/.kube/config
export CONTEXT=arn:aws:eks:eu-central-1:ACCOUNTNUMBER:cluster/meetup
kubectl config set-context $CONTEXT --namespace=meetup
```

## Create ConfigMap
```
cd terraform
terraform output config_map_aws_auth | kubectl apply -f -
kubectl get nodes
```

## Helm
```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'      
helm init --service-account tiller --upgrade

helm list 
# should output nothing
```

## Weave Flux

```bash

# Replace with your repository
export GIT_REPO=ssh://git@gitlab.com/age-of-devops/flux.git

helm repo add weaveworks https://weaveworks.github.io/flux
helm repo update

helm --kube-context=$CONTEXT upgrade -i flux \
    --set helmOperator.create=true \
    --set helmOperator.createCRD=true \
    --set git.url=$GIT_REPO \
    --set git.pollInterval=30s \
    --namespace flux \
    weaveworks/flux
```

## Add deploy key to Git repo

```bash
kubectl -n flux logs deployment/flux | grep identity.pub | cut -d '"' -f2 | pbcopy
```

Then configure your Git repo to accept this public key 

## Flux

```bash
# Flux output
kubectl -n flux logs deployment/flux -f

# Helm Operator output
kubectl -n flux logs deployment/flux-helm-operator -f
```
