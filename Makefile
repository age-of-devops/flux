.PHONY: terraform-apply terraform-init

terraform-init:
	cd terraform && terraform init

terraform-apply: terraform-init
	cd terraform && terraform apply -auto-approve -var-file=settings.tfvars

terraform-destroy:
	cd terraform && terraform destroy -var-file=settings.tfvars
